FROM python:3.8

WORKDIR .

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

ENV PYTHONBUFFERED=1

ENTRYPOINT ["python", "service.py"]