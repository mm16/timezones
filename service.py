from flask import Flask
from flask import jsonify
from flask import request

from timezones import timezone

app = Flask(__name__)


@app.route("/timezones")
def zone():
    """Endpoint for timezones.
    get:
        summary: Get timezones.
        description: Get timezone for single coordinate or list of all.
        parameters:
            - name: lon
              description: Longitude in degrees in range [-180, 180].
              type: float
              required: false
            - name: lat
              description: Latitude in degrees in range [-90, 90].
              type: float
              required: false
        responses:
            200:
                description: json object with fields:
                    - name: error
                      type: bool
                    - name: timezone
                      type: List[str] (if error: [""]).
                    - name: error_message
                      type: str (if error: "").

    """
    lon = request.args.get("lon", type=float)  # None if float() fails
    lat = request.args.get("lat", type=float)
    app.logger.debug(f"Request timezone lon = {lon}, lat = {lat}")
    try:
        if lon is None and lat is None:
            names = timezone.all_timezones()
            response = {"error": False, "timezone": names, "error_message": ""}
        else:
            time_zone = timezone.coordinates_to_timezone(lon, lat)
            response = {
                "error": False,
                "timezone": [time_zone],
                "error_message": "",
            }
    except Exception as e:
        response = {"error": True, "timezone": [""], "error_message": str(e)}
    return jsonify(response)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
