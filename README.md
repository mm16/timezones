# Timezones

Web service to return timezone from coordinates. Development server! Do not use in production.
Tested on Python 3.7 and 3.8.

# Setup instructions

    git clone https://gitlab.com/mm16/timezones.git
    cd timezones

## Running in docker container

    docker image build -t "timezones" .
    docker container run -it timezones:latest

## Running on host
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python service.py  # Ensure active venv

## Requests

The entry point for all requests is `/timezones`.

### List timezones

To return all timezones, call the entry point without parameters:

    curl "localhost:5000/timezones"
    {
      "error": false,
      "error_message": "",
      "timezone": [
        "Africa/Abidjan",
        "Africa/Accra",
        ...
        "Etc/GMT+12"
      ]
    }

### Timezone based on coordinate

To return a single timezone, pass following parameters to the entry point:

Parameter | Coordinate system | Type  | Range
---|-------------------|:-----:|:---:
lon| EPSG:4326         | float |   [-180, 180]
lat| EPSG:4326         | float |   [-90, 90]

```
curl "localhost:5000/timezones?lat=48.19&lon=16.39"
{
  "error": false,
  "error_message": "",
  "timezone": [
    "Europe/Vienna"
  ]
}
```

# Returns

The response is a JSON with following fields:

Field name | Type
---|:---:
error | bool
error_message | str
timezone | list of str

# Run tests

    python -m pytest  # Ensure active venv
