from typing import List

from timezonefinder import TimezoneFinder

TF = TimezoneFinder()


def coordinates_to_timezone(lon: float, lat: float) -> str:
    """Longitude/latitude in degrees to time zone"""
    return TF.timezone_at(lng=lon, lat=lat)


def all_timezones() -> List[str]:
    """Get list of all timezones"""
    return TF.timezone_names
