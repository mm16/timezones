import pytest

from service import app


@pytest.fixture
def app_():
    with app.test_client() as test_client:
        yield test_client


def test_timezones_all(app_):
    response = app_.get("/timezones")
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data["error"] is False
    assert json_data["error_message"] == ""
    assert isinstance(json_data["timezone"], list)
    assert len(json_data["timezone"]) > 0


def test_timezones_single(app_):
    response = app_.get("/timezones?lat=48.19&lon=16.39")
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data["error"] is False
    assert json_data["error_message"] == ""
    assert json_data["timezone"] == ["Europe/Vienna"]


def test_timezone_wrong_input_number_or_type(app_):
    wrong_requests = (
        "/timezones?lat=48.19",
        "/timezones?lon=100",
        "/timezones?lat=foo&lon=16.39",
        "/timezones?lat=3.1&lon=foo",
    )
    for request in wrong_requests:
        response = app_.get(request)
        assert response.status_code == 200
        json_data = response.get_json()
        assert json_data["error"] is True
        assert json_data["error_message"].startswith("No matching definition")
        assert json_data["timezone"] == [""]


def test_timezone_out_of_range(app_):
    out_of_range_requests = (
        "/timezones?lat=90.001&lon=100",
        "/timezones?lat=-90.001&lon=100",
        "/timezones?lat=10&lon=-181",
        "/timezones?lat=10&lon=181",
    )
    for request in out_of_range_requests:
        response = app_.get(request)
        assert response.status_code == 200
        json_data = response.get_json()
        assert json_data["error"] is True
        assert "out of bounds" in json_data["error_message"]
        assert json_data["timezone"] == [""]
