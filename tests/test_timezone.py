import pytest

from timezones import timezone


@pytest.mark.parametrize("lat", [90, 10, 0, -11, -90])
@pytest.mark.parametrize("lon", [-180, -5, 0, 5, 180])
def test_coordinates_to_timezone(lon, lat):
    got_timezone = timezone.coordinates_to_timezone(lon, lat)
    assert isinstance(got_timezone, str)


def test_coordinates_to_timezone_vienna():
    expected_timezone = "Europe/Vienna"
    lon = 16.39
    lat = 48.19
    assert timezone.coordinates_to_timezone(lon, lat) == expected_timezone


@pytest.mark.parametrize(
    "lon,lat", [(-182, 0), (180.1, 0), (0, 90.1), (0, -90.1)]
)
def test_coordinates_to_timezone_outside_range_raise(lon, lat):
    exp_error = "The coordinates should.*out of bounds."
    with pytest.raises(ValueError, match=exp_error):
        timezone.coordinates_to_timezone(lon, lat)


def test_coordinates_to_timezone_wrong_type_raise():
    with pytest.raises(TypeError):
        timezone.coordinates_to_timezone("foo", "bar")


def test_all_timezones():
    names = timezone.all_timezones()
    assert isinstance(names, list)
    assert all([isinstance(x, str) for x in names])
